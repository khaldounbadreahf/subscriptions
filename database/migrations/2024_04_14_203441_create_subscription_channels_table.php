<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('subscription_channels', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subscriptions_id')->constrained('subscriptions')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('channels_id')->constrained('channels')->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('subscription_channels');
    }
};
