<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\IsAdmin;
use App\Http\Middleware\IsAuthenticated;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;


Route::prefix('users')->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('register', [UserController::class, 'register']);
        Route::post('login', [UserController::class, 'login']);
        Route::put('logout', [UserController::class, 'logout'])->middleware(IsAuthenticated::class);
        Route::get('profile', [UserController::class, 'getUser'])->middleware(IsAuthenticated::class);
    });

    Route::prefix('channels')->middleware(IsAuthenticated::class)->group(function () {
        Route::get('/', [UserController::class, 'getAllChannels']);
        Route::post('/{channel}', [UserController::class, 'subscriptChannel']);
    });

    Route::prefix('subscriptions')->middleware(IsAuthenticated::class)->group(function () {
        Route::get('/', [UserController::class, 'getAllSubscriptions']);
        Route::post('/{subscription}', [UserController::class, 'subscripBundle']);
    });
});

Route::prefix('admins')->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('login', [AdminController::class, 'login']);
        Route::put('logout', [AdminController::class, 'logout'])->middleware(IsAdmin::class);
        Route::get('profile', [AdminController::class, 'getAdmin'])->middleware(IsAdmin::class);
    });

    Route::prefix('channels')->middleware(IsAdmin::class)->group(function () {
        Route::post('/', [AdminController::class, 'createChannel']);
        Route::get('/', [AdminController::class, 'getAllChannels']);
        Route::put('/status/{channel}', [AdminController::class, 'changeChannelStatus']);
        Route::put('/{channel}', [AdminController::class, 'updateChannel']);
    });

    Route::prefix('subscriptions')->middleware(IsAdmin::class)->group(function () {
        Route::post('/', [AdminController::class, 'createSubscription']);
        Route::put('/{subscription}', [AdminController::class, 'updateSubscription']);
        Route::put('/status/{subscription}', [AdminController::class, 'changeSubscriptionStatus']);
        Route::delete('/{channel}', [AdminController::class, 'deleteChannelFromSubscription']);
        Route::post('/channels/{subscription}', [AdminController::class, 'addChannelToSubscription']);
        Route::get('/', [AdminController::class, 'getAllSubscriptions']);
    });

    Route::prefix('users')->middleware(IsAdmin::class)->group(function () {
        Route::get('/', [AdminController::class, 'getAllUsers']);
    });
});


Route::post('seed',function (){
    Artisan::call('db:seed');
});