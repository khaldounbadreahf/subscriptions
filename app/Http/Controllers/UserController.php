<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\Channels;
use App\Models\Subscriptions;
use App\Services\Services;
use App\Services\User\UserAuthenticationServices;
use App\Services\User\UserChannelServices;
use App\Services\User\UserSubscriptionServices;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function register(UserRegisterRequest $request)
    {
        return Services::execute(function () use ($request) {
            return UserAuthenticationServices::register($request);
        });
    }

    public function login(UserLoginRequest $request)
    {
        return Services::execute(function () use ($request) {
            return UserAuthenticationServices::login($request);
        });
    }

    public function logout(Request $request)
    {
        return Services::execute(function () use ($request) {
            return UserAuthenticationServices::logout($request);
        });
    }

    public function getUser(Request $request)
    {
        return Services::execute(function () use ($request) {
            return UserAuthenticationServices::getUser($request);
        });
    }

    public function getAllChannels(Request $request)
    {
        return Services::execute(function () use ($request) {
            return UserChannelServices::getAllChannel($request);
        });
    }

    public function getAllSubscriptions(Request $request)
    {
        return Services::execute(function () use ($request) {
            return UserSubscriptionServices::getAllSubscriptions($request);
        });
    }

    public function subscriptChannel(Request $request, Channels $channel)
    {
        return Services::execute(function () use ($request, $channel) {
            return UserChannelServices::subscriptChannel($request, $channel);
        });
    }

    public function subscripBundle(Request $request, Subscriptions $subscription)
    {
        return Services::execute(function () use ($request, $subscription) {
            return UserSubscriptionServices::subscripBundle($request, $subscription);
        });
    }
}
