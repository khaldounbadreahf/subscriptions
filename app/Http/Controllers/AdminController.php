<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddChannelToSubscriptionRequest;
use App\Http\Requests\AdminLoginRequest;
use App\Http\Requests\CreateChannelRequest;
use App\Http\Requests\CreateSubscriptionRequest;
use App\Models\Channels;
use App\Models\SubscriptionChannels;
use App\Models\Subscriptions;
use App\Services\Admin\AdminAuthenticationServices;
use App\Services\Admin\AdminChannelServices;
use App\Services\Admin\AdminSubscriptionServices;
use App\Services\Admin\AdminUserServices;
use App\Services\Services;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function login(AdminLoginRequest $request)
    {
        return Services::execute(function () use ($request) {
            return AdminAuthenticationServices::login($request);
        });
    }

    public function logout(Request $request)
    {
        return Services::execute(function () use ($request) {
            return AdminAuthenticationServices::logout($request);
        });
    }

    public function getAdmin(Request $request)
    {
        return Services::execute(function () use ($request) {
            return AdminAuthenticationServices::getAdmin($request);
        });
    }

    public function getAllChannels(Request $request)
    {
        return Services::execute(function () use ($request) {
            return AdminChannelServices::getAllChannels($request);
        });
    }

    public function changeChannelStatus(Channels $channel)
    {
        return Services::execute(function () use ($channel) {
            return AdminChannelServices::changeChannelStatus($channel);
        });
    }

    public function updateChannel(Request $request, Channels $channel)
    {
        return Services::execute(function () use ($request, $channel) {
            return AdminChannelServices::updateChannel($request, $channel);
        });
    }

    public function createChannel(CreateChannelRequest $request)
    {
        return Services::execute(function () use ($request) {
            return AdminChannelServices::createChannel($request);
        });
    }

    public function createSubscription(CreateSubscriptionRequest $request)
    {
        return Services::execute(function () use ($request) {
            return AdminSubscriptionServices::createSubscription($request);
        });
    }

    public function updateSubscription(Request $request, Subscriptions $subscription)
    {
        return Services::execute(function () use ($request, $subscription) {
            return AdminSubscriptionServices::updateSubscription($request, $subscription);
        });
    }

    public function changeSubscriptionStatus(Subscriptions $subscription)
    {
        return Services::execute(function () use ($subscription) {
            return AdminSubscriptionServices::changeSubscriptionStatus($subscription);
        });
    }

    public function deleteChannelFromSubscription(SubscriptionChannels $channel)
    {
        return Services::execute(function () use ($channel) {
            return AdminSubscriptionServices::deleteChannelFromSubscription($channel);
        });
    }

    public function addChannelToSubscription(AddChannelToSubscriptionRequest $request, Subscriptions $subscription)
    {
        return Services::execute(function () use ($request, $subscription) {
            return AdminSubscriptionServices::addChannelToSubscription($request, $subscription);
        });
    }

    public function getAllSubscriptions(Request $request)
    {
        return Services::execute(function () use ($request) {
            return AdminSubscriptionServices::getAllSubscriptions($request);
        });
    }

    public function getAllUsers(Request $request)
    {
        return Services::execute(function () use ($request) {
            return AdminUserServices::getAllUsers($request);
        });
    }
}
