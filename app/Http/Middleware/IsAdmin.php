<?php

namespace App\Http\Middleware;

use App\Services\AuthServices;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $admin = AuthServices::getAdmin($request->bearerToken());
        if($admin == null){
            return \App\Responses\Response::Error("Unauthenticated",403);
        }
        $request->merge(['admin' => $admin]);
        return $next($request);
    }
}
