<?php

namespace App\Http\Middleware;

use App\Services\AuthServices;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IsAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = AuthServices::getUser($request->bearerToken());
        if($user == null){
            return \App\Responses\Response::Error("Unauthenticated",403);
        }
        $request->merge(['user' => $user]);
        return $next($request);
    }
}
