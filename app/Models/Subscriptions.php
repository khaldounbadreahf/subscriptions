<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Subscriptions extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'price',
        'is_active'
    ];

    public function channels() : HasMany
    {
        return $this->hasMany(SubscriptionChannels::class);
    }

    public function subscripable(): MorphMany
    {
        return $this->morphMany(UserSubscriptions::class, 'subscripable');
    }
}
