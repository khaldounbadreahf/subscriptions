<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class UserSubscriptions extends Model
{
    use HasFactory;
    protected $fillable = [
        'users_id',
        'subscripable_type',
        'subscripable_id'
    ];


    public function user():BelongsTo
    {
        return $this->belongsTo(Users::class,'users_id');
    }


    public function subscripable(): MorphTo
    {
        return $this->morphTo();
    }

}
