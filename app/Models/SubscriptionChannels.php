<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SubscriptionChannels extends Model
{
    use HasFactory;
    protected $fillable = [
        'subscriptions_id',
        'channels_id'
    ];

    public function subscription() : BelongsTo
    {
        return $this->belongsTo(Subscriptions::class,'subscriptions_id');
    }

    public function channel() : BelongsTo
    {
        return $this->belongsTo(Channels::class,'channels_id');
    }
}
