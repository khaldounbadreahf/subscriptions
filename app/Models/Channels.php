<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Facades\DB;

class Channels extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'price',
        'is_active'
    ];


    public function getSubscriptions(Channels $channel)
    {
        return DB::table('subscriptions')
            ->join('subscriptionChannels', 'subscriptions.id', '=', 'subscriptionChannels.subscription_id')
            ->select('subscriptions.id', 'subscriptions.name')
            ->where('subscriptionChannels.channel_id', $channel->id)
            ->get();
    }

    public function subscripable(): MorphMany
    {
        return $this->morphMany(UserSubscriptions::class, 'subscripable');
    }
}
