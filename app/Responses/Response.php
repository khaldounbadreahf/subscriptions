<?php

namespace App\Responses;

class Response {
    public static function Success( $data, $message, $code = 200 ) {
        return response()->json( [
            'code' => (int)$code,
            'message' => $message,
            'data' => $data
        ], $code );
    }

    public static function Error( $message, $code, $data=[] ) {
        return response()->json( [
            'code' => (int)$code,
            'message' => $message,
            'data' => $data

        ], $code );
    }

    public static function ValidationError( $validator ) {
        $errors = collect($validator->errors())
            ->flatMap(function ($messages, $field) {
                return [$field => $messages];
            })
            ->map(function ($messages, $field) {
                return [
                    'error' => $messages[0],
                    'field' => $field,
                ];
            })
            ->values();
        return response()->json( [
            'code' => 405,
            'message' => $errors[0]['error'],
            'data' => []
        ], 405 );
    }
}
