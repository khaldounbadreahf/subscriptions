<?php

namespace App\Services\Admin;

use App\Http\Requests\AddChannelToSubscriptionRequest;
use App\Http\Requests\CreateSubscriptionRequest;
use App\Models\SubscriptionChannels;
use App\Models\Subscriptions;
use App\Responses\Response;
use App\Services\AdminResponse;
use Illuminate\Http\Request;

class AdminSubscriptionServices
{
    public static function createSubscription(CreateSubscriptionRequest $request)
    {
        $subscription = Subscriptions::create($request->all());

        $subscription->channels()->attach($request->channels);

        return Response::Success(AdminResponse::subscriptionResponse($subscription), 'Subscription Created Successfully');
    }

    public static function updateSubscription(Request $request, Subscriptions $subscription)
    {
        $subscription->update($request->all());

        return Response::Success(AdminResponse::subscriptionResponse($subscription), 'Subscription Updated Successfully');
    }

    public static function changeSubscriptionStatus(Subscriptions $subscription)
    {
        $subscription->update([
            'is_active' => !$subscription->is_active
        ]);

        return Response::Success(AdminResponse::subscriptionResponse($subscription), 'Change Status Subscription Successfully');
    }

    public static function deleteChannelFromSubscription(SubscriptionChannels $channel)
    {
        $channel->forceDelete();

        return Response::Success(AdminResponse::subscriptionResponse($channel->subscription), 'Channel Deleted Successfully');
    }

    public static function addChannelToSubscription(AddChannelToSubscriptionRequest $request, Subscriptions $subscription)
    {
        $subscription->channels()->attach($request->channels);


        return Response::Success(AdminResponse::subscriptionResponse($subscription), 'Channels Added Successfully');
    }

    public static function getAllSubscriptions(Request $request)
    {
        $data = [];

        $perPage = $request->input('count', 10);

        $subscriptions = Subscriptions::paginate($perPage);

        $data = $subscriptions->map(function ($subscription) {
            return AdminResponse::subscriptionResponse($subscription);
        });

        return Response::Success([
            'totla' => $subscriptions->lastPage(),
            'subscriptions' => $data
        ], 'Get All Subscriptions Successfully');
    }
}
