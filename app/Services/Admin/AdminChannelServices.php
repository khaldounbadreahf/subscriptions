<?php

namespace App\Services\Admin;

use App\Http\Requests\CreateChannelRequest;
use App\Models\Channels;
use App\Responses\Response;
use App\Services\AdminResponse;
use Illuminate\Http\Request;

class AdminChannelServices
{
    public static function createChannel(CreateChannelRequest $request)
    {
        $cahnnel = Channels::create($request->all());

        return Response::Success(AdminResponse::channelResponse($cahnnel), 'Channel Created Successfully');
    }

    public static function getAllChannels(Request $request)
    {
        $data = [];
        $perPage = $request->input('count', 10);
        $channels = Channels::paginate($perPage);

        $data = $channels->map(function ($channel) {
            return AdminResponse::channelResponse($channel);
        });

        return Response::Success([
            'total_page' => $channels->lastPage(),
            'channels' => $data
        ], 'Get All Channels Successfully');
    }

    public static function changeChannelStatus(Channels $channel)
    {
        $channel->update([
            'is_active' => !$channel->is_active
        ]);

        return Response::Success(AdminResponse::channelResponse($channel), 'Channel Status Updated Successfully');
    }

    public static function updateChannel(Request $request, Channels $channel)
    {
        $channel->update($request->all());

        return Response::Success(AdminResponse::channelResponse($channel), 'Channel Updated Successfully');
    }
}
