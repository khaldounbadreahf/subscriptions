<?php

namespace App\Services\Admin;

use App\Http\Requests\AdminLoginRequest;
use App\Models\Admins;
use App\Responses\Response;
use App\Services\AdminResponse;
use App\Services\AuthServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminAuthenticationServices
{
    public static function login(AdminLoginRequest $request)
    {
        $admin = Admins::where('email', $request->email)->first();

        if (Hash::check($request->password, $admin->password)) {
            $admin->update([
                'token' => AuthServices::generateToken($admin->id)
            ]);
            return Response::Success(AdminResponse::authResponse($admin), 'Success');
        }
        return Response::Error('Password not corect', 401);
    }


    public static function logout(Request $request)
    {
        $admin = $request->input('admin');
        $admin->update([
            'token' => ''
        ]);

        return Response::Success([], 'Admin Logout Successfully');
    }

    public static function getAdmin(Request $request)
    {
        $admin = $request->input('admin');
        return Response::Success(AdminResponse::adminResponse($admin), 'Get Admin Profile Succcess');
    }
}
