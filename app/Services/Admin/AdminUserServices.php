<?php

namespace App\Services\Admin;

use App\Models\Users;
use App\Responses\Response;
use App\Services\AdminResponse;
use Illuminate\Http\Request;

class AdminUserServices
{
    public static function getAllUsers(Request $request)
    {
        $data = [];

        $perPage = $request->input('count', 10);

        $users = Users::paginate($perPage);

        $data = $users->map(function ($user) {
            return AdminResponse::userResponse($user);
        });

        return Response::Success([
            'total' => $users->lastPage(),
            'users' => $data
        ], 'Get All Users Successfully');
    }
}
