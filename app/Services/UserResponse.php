<?php

namespace App\Services;

use App\Models\Channels;
use App\Models\Subscriptions;
use App\Models\Users;

class UserResponse
{
    public static function authResponse(Users $user)
    {
        return [
            'access_token' => $user->token,
            'user' => self::userResponse($user)
        ];
    }

    public static function userResponse(Users $user)
    {
        $channels = [];
        $bundles = [];

        foreach ($user->subscripable()->get() as $subscription) {
            if ($subscription->subscripable_type === Subscriptions::class) {
                $bundles[] = self::subscriptionResponse($subscription->subscripable);
            } else {
                $channels[] = self::channelResponse($subscription->subscripable);
            }
        }
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'subscriptions' => [
                'channels' => $channels,
                'bundles' => $bundles,
            ]
        ];
    }

    public static function channelResponse(Channels $channel)
    {
        return [
            'id' => $channel->id,
            'name' => $channel->name,
            'price' => (float) $channel->price
        ];
    }

    public static function subscriptionResponse(Subscriptions $subscription)
    {
        $channels = [];
        foreach ($subscription->channels()->get() as $channel) {
            $channels[] = [
                'id' => $channel->id,
                'channel' => self::channelResponse($channel->channel)
            ];
        }
        return [
            'id' => $subscription->id,
            'name' => $subscription->name,
            'price' => (float) $subscription->price,
            'channels' => $channels
        ];
    }
}
