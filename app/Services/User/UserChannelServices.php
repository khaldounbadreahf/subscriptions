<?php

namespace App\Services\User;

use App\Models\Channels;
use App\Responses\Response;
use App\Services\UserResponse;
use Illuminate\Http\Request;

class UserChannelServices
{
    public static function getAllChannel(Request $request)
    {
        $data = [];
        $perPage = $request->input('count', 10);
        $channels = Channels::where('is_active', true)->paginate($perPage);

        $data = $channels->map(function ($channel) {
            return UserResponse::channelResponse($channel);
        });

        return Response::Success([
            'total' => $channels->lastPage(),
            'channels' => $data
        ], 'Get Channels Successfully');
    }

    public static function subscriptChannel(Request $request, Channels $channel)
    {
        $user = $request->input('user');

        if ($user->subscripable()->where('subscripable_id', $channel->id)
            ->where('subscripable_type', Channels::class)->exists()
        ) {
            return Response::Error('You are already subscribed to this channel', 400);
        }

        $user->subscripable()->create([
            'subscripable_type' => Channels::class,
            'subscripable_id' => $channel->id,
        ]);

        return Response::Success(UserResponse::channelResponse($channel), 'Success');
    }
}
