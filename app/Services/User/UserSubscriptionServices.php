<?php

namespace App\Services\User;

use App\Models\Channels;
use App\Models\Subscriptions;
use App\Models\Users;
use App\Responses\Response;
use App\Services\UserResponse;
use Illuminate\Http\Request;

class UserSubscriptionServices
{
    public static function getAllSubscriptions(Request $request)
    {
        $data = [];

        $perPage = $request->input('count', 10);

        $subscriptions = Subscriptions::where('is_active', true)->paginate($perPage);

        $data = $subscriptions->map(function ($subscription) {
            return UserResponse::subscriptionResponse($subscription);
        });

        return Response::Success([
            'totla' => $subscriptions->lastPage(),
            'subscriptions' => $data
        ], 'Get All Subscriptions Successfully');
    }

    public static function subscripBundle(Request $request, Subscriptions $subscription)
    {
        $user = $request->input('user');

        if ($user->subscripable()->where('subscripable_id', $subscription->id)
            ->where('subscripable_type', Subscriptions::class)->exists()
        ) {
            return Response::Error('You have already subscribed to this bundle', 400);
        }

        if(self::isUserSubscribedToBundle($user,$subscription)){
            return Response::Error('Channel Or More From This Bundle You Are Subscriped It', 400);
        }

        $user->subscripable()->create([
            'subscripable_type' => Subscriptions::class,
            'subscripable_id' => $subscription->id,
        ]);

        return Response::Success(UserResponse::subscriptionResponse($subscription), 'Success');
    }

    public static function isUserSubscribedToBundle(Users $user, Subscriptions $subscription)
    {
        foreach ($subscription->channels()->get() as $channel) {
            if ($user->subscripable()->where('subscripable_id', $channel->id)
                ->where('subscripable_type', Channels::class)->exists()
            ) {
                return true;
            }
        }

        return false;
    }
}
