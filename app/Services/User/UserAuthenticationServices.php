<?php

namespace App\Services\User;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\Users;
use App\Responses\Response;
use App\Services\AuthServices;
use App\Services\UserResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserAuthenticationServices
{
    public static function register(UserRegisterRequest $request)
    {
        $user = Users::create($request->all());
        $user->update([
            'token' => AuthServices::generateToken($user->id)
        ]);
        return Response::Success(UserResponse::authResponse($user), 'User Registered Successfully');
    }

    public static function login(UserLoginRequest $request)
    {
        $user = Users::where('email', $request->email)->first();

        if (Hash::check($request->password, $user->password)) {
            $user->update([
                'token' => AuthServices::generateToken($user->id)
            ]);
            return Response::Success(UserResponse::authResponse($user), 'Success');
        }
        return Response::Error('Password not corect', 401);
    }

    public static function logout(Request $request)
    {
        $user = $request->input('user');
        $user->update([
            'token' => ''
        ]);

        return Response::Success([], 'User Logout Successfully');
    }

    public static function getUser(Request $request)
    {
        $user = $request->input('user');
        return Response::Success(UserResponse::userResponse($user), 'Get User Profile Succcess');
    }
}
