<?php

namespace App\Services;

use App\Responses\Response;
use Illuminate\Support\Facades\Request;
use Exception;

class Services
{
    public static function execute($function)
    {
        try {
            return $function();
        } catch (Exception $e) {
            $url = Request::fullUrl();
            return Response::Error("Error at:$url/n " . $e->getMessage() . ' at line: ' . $e->getLine() . ' in file ' . $e->getFile(), 500);
        }
    }
}