<?php

namespace App\Services;

use App\Models\Admins;
use App\Models\Channels;
use App\Models\Subscriptions;
use App\Models\Users;

class AdminResponse{
    public static function authResponse(Admins $admin){
        return [
            'access_token' => $admin->token,
            'admin' => self::adminResponse($admin)
        ];
    }

    public static function adminResponse(Admins $admin){
        return [
            'id' => $admin->id,
            'name' => $admin->name,
            'email' => $admin->email
        ];
    }

    public static function channelResponse(Channels $channel){
        return [
            'id' => $channel->id,
            'name' => $channel->name,
            'price' => (float) $channel->price,
            'is_active' => (bool) $channel->is_active
        ];
    }

    public static function subscriptionResponse(Subscriptions $subscription){
        $channels = [];
        foreach($subscription->channels()->get() as $channel){
            $channels[] = [
                'id' => $channel->id,
                'channel' => self::channelResponse($channel->channel)
            ];
        }
        return [
            'id' => $subscription->id,
            'name' => $subscription->name,
            'price' => (float) $subscription->price,
            'is_active' => (bool) $subscription->is_active,
            'channels' => $channels
        ];
    }

    public static function userResponse(Users $user){
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'created_at' => $user->created_at
        ];
    }
}