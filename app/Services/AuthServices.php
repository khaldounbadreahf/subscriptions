<?php

namespace App\Services;

use App\Models\Admins;
use App\Models\Users;
use Illuminate\Support\Str;


class AuthServices
{
    public static function getUser($token)
    {
        if ($token == null) {
            return null;
        }

        $user = Users::where('token', $token)->first();
        if ($user == null) {
            return null;
        }

        return $user;
    }


    public static function getAdmin($token)
    {
        if ($token == null) {
            return null;
        }

        $admin = Admins::where('token', $token)->first();
        if ($admin == null) {
            return null;
        }

        return $admin;
    }


    public static function generateToken($id): string
    {
        $token = Str::random(63 - Str::length((string) $id));
        $token = (string) $id . '|' . $token;
        return $token;
    }

}